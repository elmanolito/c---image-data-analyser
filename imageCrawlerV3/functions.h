void html_body_upper(FILE *file, char* input)
{
    fprintf(file, "<!doctype html>\n");
    fprintf(file, "<html>\n");
    fprintf(file, "\t<head>\n");
    fprintf(file, "<link rel=\"stylesheet\" href=\"style.css\">");
    fprintf(file, "<link href=\"https://fonts.googleapis.com/css?family=Roboto\" rel=\"stylesheet\">");
    fprintf(file, "\t</head>\n");
    fprintf(file, "\t<body>\n");
    fprintf(file, "\t\t<h1>FOLDER CRAWLER 9001 v.01</h1>");
    fprintf(file, "\t\t<h2>%s</h2>", input);

}

void html_body_lower(FILE *file)
{
    fprintf(file, "\t</body>\n");
    fprintf(file, "</html>\n");
}

long int file_size(char* file_name)
{
    FILE *fp=fopen(file_name, "r");

    if(!fp)
    {
        printf("File not found!\n");
        return -1;
    }
    fseek(fp, 0L, SEEK_END);
    long result=ftell(fp);
    fclose(fp);
    return result;
}
