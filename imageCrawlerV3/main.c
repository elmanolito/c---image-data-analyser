/*
    =====================================
    -------FolderImageCrawler 9001-------
    =====================================
    Author:     Manuel Gall
    Created:    12/12/2018
    Last edit:  18/12/2018
    =====================================
*/
#include <stdio.h>
#include <stdlib.h>
#include <dirent.h>
#include <string.h>
#include "functions.h"
#define MAXBILDER 1000


typedef enum
{
    JPG=0,GIF=1,PNG=2,UNBEKANNT=3
} Filetype;

const char FILETYPENAMES[4][10]={"JPG","GIF","PNG","UNBEKANNT"};

typedef struct
{
    char file_path[256];
    char filename[50];
    Filetype filetype;
    int height;
    int width;
    int sizeInByte;
} CheckIMGstruct;

CheckIMGstruct img[MAXBILDER];
int image_counter=0;
char userInput[100];


int main()
{
    // User auffordern einen Verzeichnisnamen einzugeben
    printf("Bitte Verzeichnisnamen eingeben: ");
    scanf("%s", userInput);

    // Verzeichnis �ffnen
    DIR *dir=opendir(userInput);
    struct dirent *dirptr;

    // Erstellen einer HTML Datei mit dem Namen: "Bilder_ + userInput + .html"
    char html_file_name[100] = "bilder_";
    strcat(html_file_name, userInput);
    strcat(html_file_name, ".html");
    FILE *html = fopen(html_file_name, "wt");
    if(!html)
    {
        printf("konnte Datei nicht schreiben");
        return -1;
    }

    // ersten Teil vom HTML in die Datei schreiben
    html_body_upper(html, userInput);

    // Verzeichnis Eintrag f�r Eintrag lesen
    while((dirptr=readdir(dir))!=NULL)
    {
        /*
            ----------------
            BILDVERARBEITUNG
            ----------------
        */

        // DATEINAME
        if(strcmp(dirptr->d_name, ".")==0 || strcmp(dirptr->d_name, "..")==0)
        {
            continue;
        }
        // Der Dateiname wird f�r jede Datei in "filename" vom struct gepackt
        strcpy(img[image_counter].filename, dirptr->d_name);
        // Der Dateipfad wird f�r jede Datei in "file_path" vom struct gepackt
        strcpy(img[image_counter].file_path, userInput);
        strcat(img[image_counter].file_path, "/");
        strcat(img[image_counter].file_path, dirptr->d_name);

        // DATEITYP
        char* fileExt=strrchr(img[image_counter].filename, '.');
        if(!fileExt)
        {
            // keine Dateiendung
        }
        else
        {
            img[image_counter].filetype=strcmp(fileExt+1,"jpeg")==0 || strcmp(fileExt+1, "jpg")==0 || strcmp(fileExt+1, "JPEG")==0 || strcmp(fileExt+1, "JPG")==0 ? JPG :
                 strcmp(fileExt+1, "png")==0 || strcmp(fileExt+1, "PNG")==0 ? PNG :
                 strcmp(fileExt+1, "gif")==0 || strcmp(fileExt+1, "GIF")==0 ? GIF :
                 UNBEKANNT;
        }

        // DATEIGR��E
        img[image_counter].sizeInByte=file_size(img[image_counter].file_path);
        printf("%s\n", img[image_counter].filename);

        // FORMAT

        // Wenn es ein PNG ist
        if(img[image_counter].filetype==PNG)
        {
            FILE *png_file=fopen(img[image_counter].file_path, "rb");
            int dimensions[4];
            for(int i=0;i<16;i++)
            {
                fgetc(png_file); // die ersten 16 Bytes ignorieren
            }
            for(int j=0;j<8;j++)
            {
                dimensions[j]=fgetc(png_file); // die relevanten Bytes 6-9 in einem Array speichern
            }
            img[image_counter].width=(dimensions[0]<<24) | (dimensions[1]<<16) | (dimensions[2]<<8) | dimensions[3];
            img[image_counter].height=(dimensions[4]<<24) | (dimensions[5]<<16) | (dimensions[6]<<8) | dimensions[7];

            fclose(png_file);
        }

        // Wenn es ein GIF ist:
        if(img[image_counter].filetype==GIF)
        {
            FILE *gif_file=fopen(img[image_counter].file_path, "rb");
            int dimensions[4];
            for(int i=0;i<6;i++)
            {
                fgetc(gif_file); // die ersten 6 Bytes ignorieren
            }
            for(int j=0;j<4;j++)
            {
                dimensions[j]=fgetc(gif_file); // die relevanten Bytes 6-9 in einem Array speichern
            }
            // hier ist es wichtig die Bytes zu vertauschen, da GIF etwas eigen ist
            img[image_counter].width=dimensions[0] | (dimensions[1]<<8);
            img[image_counter].height=dimensions[2] | (dimensions[3]<<8);

            fclose(gif_file);
        }

        // Wenn es ein JPEG ist
        /*
            Die Dimensionen von einem jpeg lassen sich wohl nicht so ohne weiteres auslesen.
            Deswegen wird erst einmal darauf verzichtet.
        */



        image_counter++;
    }

    // Informationen aus den Structs in die HTML Datei schreiben
    for(int i=0; i<image_counter;i++)
    {
        fprintf(html, "\t<div class=\"box\">\n");
        fprintf(html, "\t\t%03d.\n", i+1);
        fprintf(html, "\t\t<br/><img src=\"%s/%s\" class=\"resize\">\n", userInput, img[i].filename);
        fprintf(html, "\t\t<table>\n");
        fprintf(html, "\t\t\t<tr>\n");
        fprintf(html, "\t\t\t\t<td>Titel:</td>\n");
        fprintf(html, "\t\t\t\t<td>%s</td>\n", img[i].filename);
        fprintf(html, "\t\t\t</tr>\n");
        fprintf(html, "\t\t\t<tr>\n");
        fprintf(html, "\t\t\t\t<td>Dateityp:</td>\n");
        fprintf(html, "\t\t\t\t<td>%s</td>\n", FILETYPENAMES[img[i].filetype]);
        fprintf(html, "\t\t\t</tr>\n");
        fprintf(html, "\t\t\t<tr>\n");
        fprintf(html, "\t\t\t\t<td>H&ouml;he:</td>\n");
        fprintf(html, "\t\t\t\t<td>%d px</td>\n", img[i].height);
        fprintf(html, "\t\t\t</tr>\n");
        fprintf(html, "\t\t\t<tr>\n");
        fprintf(html, "\t\t\t\t<td>Breite:</td>\n");
        fprintf(html, "\t\t\t\t<td>%d px</td>\n", img[i].width);
        fprintf(html, "\t\t\t</tr>\n");
        fprintf(html, "\t\t\t<tr>\n");
        fprintf(html, "\t\t\t\t<td>Dateigr&ouml;&szlig;e:</td>\n");
        fprintf(html, "\t\t\t\t<td>%d KB</td>\n", img[i].sizeInByte/1000);
        fprintf(html, "\t\t\t</tr>\n");
        fprintf(html, "\t\t</table>\n");
        fprintf(html, "\t</div>\n");
    }

    // unteren Teil vom HTML in die Datei schreiben
    html_body_lower(html);

    fclose(html);
    closedir(dir);

    return 0;
}
